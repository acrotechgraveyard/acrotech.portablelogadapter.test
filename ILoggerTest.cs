﻿using System;
using Acrotech.PortableLogAdapter;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Acrotech.PortableLogAdapter.Test
{
    [TestClass]
    public class ILoggerTest
    {
        private const string TestLogMessage = "Test";
        private const string TestLogMessageWithParams = "Test: {0}";
        private const string TestLogMessageParam = "Param";

        private static readonly Exception TestException = new NullReferenceException("Test Exception");

        [TestMethod]
        public void TraceTest()
        {
            UnitTestLogger logger = null;

            // ensure no exceptions
            logger.Trace(TestLogMessage);
            logger.Trace(TestLogMessageWithParams, TestLogMessageParam);
            logger.Trace(() => TestLogMessage);
            logger.TraceException(TestException, TestLogMessage);
            logger.TraceException(TestException, TestLogMessageWithParams, TestLogMessageParam);
            logger.TraceException(TestException, () => TestLogMessage);
            Assert.IsFalse(logger.IsTraceEnabled());

            logger = UnitTestLogger.Default;

            logger.Trace(TestLogMessage);
            Assert.AreEqual(string.Format("[{0}] {1}", LogLevel.Trace, TestLogMessage), logger.LastOutput);

            logger.Trace(TestLogMessageWithParams, TestLogMessageParam);
            Assert.AreEqual(string.Format("[{0}] {1}", LogLevel.Trace, string.Format(TestLogMessageWithParams, TestLogMessageParam)), logger.LastOutput);

            logger.Trace(() => TestLogMessage);
            Assert.AreEqual(string.Format("[{0}] {1}", LogLevel.Trace, TestLogMessage), logger.LastOutput);

            logger.TraceException(TestException, TestLogMessage);
            Assert.AreEqual(string.Format("[{0}] {1} {2}", LogLevel.Trace, TestException.Message, TestLogMessage), logger.LastOutput);

            logger.TraceException(TestException, TestLogMessageWithParams, TestLogMessageParam);
            Assert.AreEqual(string.Format("[{0}] {1} {2}", LogLevel.Trace, TestException.Message, string.Format(TestLogMessageWithParams, TestLogMessageParam)), logger.LastOutput);

            logger.TraceException(TestException, () => TestLogMessage);
            Assert.AreEqual(string.Format("[{0}] {1} {2}", LogLevel.Trace, TestException.Message, TestLogMessage), logger.LastOutput);

            Assert.IsTrue(logger.IsTraceEnabled());
        }

        [TestMethod]
        public void DebugTest()
        {
            UnitTestLogger logger = null;

            // ensure no exceptions
            logger.Debug(TestLogMessage);
            logger.Debug(TestLogMessageWithParams, TestLogMessageParam);
            logger.Debug(() => TestLogMessage);
            logger.DebugException(TestException, TestLogMessage);
            logger.DebugException(TestException, TestLogMessageWithParams, TestLogMessageParam);
            logger.DebugException(TestException, () => TestLogMessage);
            Assert.IsFalse(logger.IsDebugEnabled());

            logger = UnitTestLogger.Default;

            logger.Debug(TestLogMessage);
            Assert.AreEqual(string.Format("[{0}] {1}", LogLevel.Debug, TestLogMessage), logger.LastOutput);

            logger.Debug(TestLogMessageWithParams, TestLogMessageParam);
            Assert.AreEqual(string.Format("[{0}] {1}", LogLevel.Debug, string.Format(TestLogMessageWithParams, TestLogMessageParam)), logger.LastOutput);

            logger.Debug(() => TestLogMessage);
            Assert.AreEqual(string.Format("[{0}] {1}", LogLevel.Debug, TestLogMessage), logger.LastOutput);

            logger.DebugException(TestException, TestLogMessage);
            Assert.AreEqual(string.Format("[{0}] {1} {2}", LogLevel.Debug, TestException.Message, TestLogMessage), logger.LastOutput);

            logger.DebugException(TestException, TestLogMessageWithParams, TestLogMessageParam);
            Assert.AreEqual(string.Format("[{0}] {1} {2}", LogLevel.Debug, TestException.Message, string.Format(TestLogMessageWithParams, TestLogMessageParam)), logger.LastOutput);

            logger.DebugException(TestException, () => TestLogMessage);
            Assert.AreEqual(string.Format("[{0}] {1} {2}", LogLevel.Debug, TestException.Message, TestLogMessage), logger.LastOutput);

            Assert.IsTrue(logger.IsDebugEnabled());
        }

        [TestMethod]
        public void InfoTest()
        {
            UnitTestLogger logger = null;

            // ensure no exceptions
            logger.Info(TestLogMessage);
            logger.Info(TestLogMessageWithParams, TestLogMessageParam);
            logger.Info(() => TestLogMessage);
            logger.InfoException(TestException, TestLogMessage);
            logger.InfoException(TestException, TestLogMessageWithParams, TestLogMessageParam);
            logger.InfoException(TestException, () => TestLogMessage);
            Assert.IsFalse(logger.IsInfoEnabled());

            logger = UnitTestLogger.Default;

            logger.Info(TestLogMessage);
            Assert.AreEqual(string.Format("[{0}] {1}", LogLevel.Info, TestLogMessage), logger.LastOutput);

            logger.Info(TestLogMessageWithParams, TestLogMessageParam);
            Assert.AreEqual(string.Format("[{0}] {1}", LogLevel.Info, string.Format(TestLogMessageWithParams, TestLogMessageParam)), logger.LastOutput);

            logger.Info(() => TestLogMessage);
            Assert.AreEqual(string.Format("[{0}] {1}", LogLevel.Info, TestLogMessage), logger.LastOutput);

            logger.InfoException(TestException, TestLogMessage);
            Assert.AreEqual(string.Format("[{0}] {1} {2}", LogLevel.Info, TestException.Message, TestLogMessage), logger.LastOutput);

            logger.InfoException(TestException, TestLogMessageWithParams, TestLogMessageParam);
            Assert.AreEqual(string.Format("[{0}] {1} {2}", LogLevel.Info, TestException.Message, string.Format(TestLogMessageWithParams, TestLogMessageParam)), logger.LastOutput);

            logger.InfoException(TestException, () => TestLogMessage);
            Assert.AreEqual(string.Format("[{0}] {1} {2}", LogLevel.Info, TestException.Message, TestLogMessage), logger.LastOutput);

            Assert.IsTrue(logger.IsInfoEnabled());
        }

        [TestMethod]
        public void WarnTest()
        {
            UnitTestLogger logger = null;

            // ensure no exceptions
            logger.Warn(TestLogMessage);
            logger.Warn(TestLogMessageWithParams, TestLogMessageParam);
            logger.Warn(() => TestLogMessage);
            logger.WarnException(TestException, TestLogMessage);
            logger.WarnException(TestException, TestLogMessageWithParams, TestLogMessageParam);
            logger.WarnException(TestException, () => TestLogMessage);
            Assert.IsFalse(logger.IsWarnEnabled());

            logger = UnitTestLogger.Default;

            logger.Warn(TestLogMessage);
            Assert.AreEqual(string.Format("[{0}] {1}", LogLevel.Warn, TestLogMessage), logger.LastOutput);

            logger.Warn(TestLogMessageWithParams, TestLogMessageParam);
            Assert.AreEqual(string.Format("[{0}] {1}", LogLevel.Warn, string.Format(TestLogMessageWithParams, TestLogMessageParam)), logger.LastOutput);

            logger.Warn(() => TestLogMessage);
            Assert.AreEqual(string.Format("[{0}] {1}", LogLevel.Warn, TestLogMessage), logger.LastOutput);

            logger.WarnException(TestException, TestLogMessage);
            Assert.AreEqual(string.Format("[{0}] {1} {2}", LogLevel.Warn, TestException.Message, TestLogMessage), logger.LastOutput);

            logger.WarnException(TestException, TestLogMessageWithParams, TestLogMessageParam);
            Assert.AreEqual(string.Format("[{0}] {1} {2}", LogLevel.Warn, TestException.Message, string.Format(TestLogMessageWithParams, TestLogMessageParam)), logger.LastOutput);

            logger.WarnException(TestException, () => TestLogMessage);
            Assert.AreEqual(string.Format("[{0}] {1} {2}", LogLevel.Warn, TestException.Message, TestLogMessage), logger.LastOutput);

            Assert.IsTrue(logger.IsWarnEnabled());
        }

        [TestMethod]
        public void ErrorTest()
        {
            UnitTestLogger logger = null;

            // ensure no exceptions
            logger.Error(TestLogMessage);
            logger.Error(TestLogMessageWithParams, TestLogMessageParam);
            logger.Error(() => TestLogMessage);
            logger.ErrorException(TestException, TestLogMessage);
            logger.ErrorException(TestException, TestLogMessageWithParams, TestLogMessageParam);
            logger.ErrorException(TestException, () => TestLogMessage);
            Assert.IsFalse(logger.IsErrorEnabled());

            logger = UnitTestLogger.Default;

            logger.Error(TestLogMessage);
            Assert.AreEqual(string.Format("[{0}] {1}", LogLevel.Error, TestLogMessage), logger.LastOutput);

            logger.Error(TestLogMessageWithParams, TestLogMessageParam);
            Assert.AreEqual(string.Format("[{0}] {1}", LogLevel.Error, string.Format(TestLogMessageWithParams, TestLogMessageParam)), logger.LastOutput);

            logger.Error(() => TestLogMessage);
            Assert.AreEqual(string.Format("[{0}] {1}", LogLevel.Error, TestLogMessage), logger.LastOutput);

            logger.ErrorException(TestException, TestLogMessage);
            Assert.AreEqual(string.Format("[{0}] {1} {2}", LogLevel.Error, TestException.Message, TestLogMessage), logger.LastOutput);

            logger.ErrorException(TestException, TestLogMessageWithParams, TestLogMessageParam);
            Assert.AreEqual(string.Format("[{0}] {1} {2}", LogLevel.Error, TestException.Message, string.Format(TestLogMessageWithParams, TestLogMessageParam)), logger.LastOutput);

            logger.ErrorException(TestException, () => TestLogMessage);
            Assert.AreEqual(string.Format("[{0}] {1} {2}", LogLevel.Error, TestException.Message, TestLogMessage), logger.LastOutput);

            Assert.IsTrue(logger.IsErrorEnabled());
        }

        [TestMethod]
        public void FatalTest()
        {
            UnitTestLogger logger = null;

            // ensure no exceptions
            logger.Fatal(TestLogMessage);
            logger.Fatal(TestLogMessageWithParams, TestLogMessageParam);
            logger.Fatal(() => TestLogMessage);
            logger.FatalException(TestException, TestLogMessage);
            logger.FatalException(TestException, TestLogMessageWithParams, TestLogMessageParam);
            logger.FatalException(TestException, () => TestLogMessage);
            Assert.IsFalse(logger.IsFatalEnabled());

            logger = UnitTestLogger.Default;

            logger.Fatal(TestLogMessage);
            Assert.AreEqual(string.Format("[{0}] {1}", LogLevel.Fatal, TestLogMessage), logger.LastOutput);

            logger.Fatal(TestLogMessageWithParams, TestLogMessageParam);
            Assert.AreEqual(string.Format("[{0}] {1}", LogLevel.Fatal, string.Format(TestLogMessageWithParams, TestLogMessageParam)), logger.LastOutput);

            logger.Fatal(() => TestLogMessage);
            Assert.AreEqual(string.Format("[{0}] {1}", LogLevel.Fatal, TestLogMessage), logger.LastOutput);

            logger.FatalException(TestException, TestLogMessage);
            Assert.AreEqual(string.Format("[{0}] {1} {2}", LogLevel.Fatal, TestException.Message, TestLogMessage), logger.LastOutput);

            logger.FatalException(TestException, TestLogMessageWithParams, TestLogMessageParam);
            Assert.AreEqual(string.Format("[{0}] {1} {2}", LogLevel.Fatal, TestException.Message, string.Format(TestLogMessageWithParams, TestLogMessageParam)), logger.LastOutput);

            logger.FatalException(TestException, () => TestLogMessage);
            Assert.AreEqual(string.Format("[{0}] {1} {2}", LogLevel.Fatal, TestException.Message, TestLogMessage), logger.LastOutput);

            Assert.IsTrue(logger.IsFatalEnabled());
        }

        class UnitTestLogger : ILogger
        {
            public static readonly UnitTestLogger Default = new UnitTestLogger();

            public string LastOutput { get; private set; }

            #region ILogger Members

            public string Name { get { return string.Empty; } }

            public void Log(LogLevel level, string format, params object[] args)
            {
                LastOutput = string.Format("[{0}] {1}", level, string.Format(format, args));
            }

            public void Log(LogLevel level, Func<string> messageCreator)
            {
                LastOutput = string.Format("[{0}] {1}", level, messageCreator());
            }

            public void LogException(LogLevel level, Exception exception, string format, params object[] args)
            {
                LastOutput = string.Format("[{0}] {1} {2}", level, exception.Message, string.Format(format, args));
            }

            public void LogException(LogLevel level, Exception exception, Func<string> messageCreator)
            {
                LastOutput = string.Format("[{0}] {1} {2}", level, exception.Message, messageCreator());
            }

            public LogLevel LogLevel { get; set; }

            #endregion
        }

    }
}
