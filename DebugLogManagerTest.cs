﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Acrotech.PortableLogAdapter;
using Acrotech.PortableLogAdapter.Managers;

namespace Acrotech.PortableLogAdapter.Test
{
    [TestClass]
    public class DebugLogManagerTest
    {
#if !DEBUG
        private static string LastMessage { get; set; }

        private static void Log(string message)
        {
            LastMessage = message;
        }

        [ClassInitialize]
        public static void TestInitialize(TestContext context)
        {
            DebugLogManager.Initialize(Log);
        }
#endif

        [TestMethod]
        public void GetLoggerNullNameTest()
        {
            var logger = DebugLogManager.Default.GetLogger(null);

            Assert.IsNotNull(logger);
            Assert.AreEqual(DebugLogManager.Logger, logger);
            Assert.AreEqual(DebugLogManager.LoggerName, logger.Name);
        }

        [TestMethod]
        public void GetLoggerNonNullNameTest()
        {
            var logger = DebugLogManager.Default.GetLogger("UnitTest");

            Assert.IsNotNull(logger);
            Assert.AreEqual(DebugLogManager.Logger, logger);
            Assert.AreEqual(DebugLogManager.LoggerName, logger.Name);
        }

        [TestMethod]
        public void GetLoggerTest()
        {
            var logger = DebugLogManager.Default.GetLogger();

            Assert.IsNotNull(logger);
            Assert.AreEqual(DebugLogManager.Logger, logger);
            Assert.AreEqual(DebugLogManager.LoggerName, logger.Name);
        }

        [TestMethod]
        public void LogToDebugLoggerTest()
        {
            var logger = DebugLogManager.Default.GetLogger();

            // just make sure we don't crash here
            logger.Debug("Testing");
        }
    }
}
