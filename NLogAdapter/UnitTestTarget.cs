﻿using NLog.Targets;
using System;

namespace Acrotech.PortableLogAdapter.Test.NLogAdapter
{
    public class UnitTestTarget : TargetWithLayout
    {
        public UnitTestTarget(Action<string> writeAction)
        {
            if (writeAction == null)
            {
                throw new NullReferenceException("Write Action Must be Non-Null");
            }

            WriteAction = writeAction;
            Layout = "[${level}] ${message}${onexception:\\: ${exception:format=tostring}}";
        }

        public Action<string> WriteAction { get; private set; }

        protected override void Write(NLog.LogEventInfo logEvent)
        {
            WriteAction(Layout.Render(logEvent));
        }
    }
}
