﻿using Acrotech.PortableLogAdapter;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acrotech.PortableLogAdapter.Test.NLogAdapter
{
    public class NLogManager : ILogManager
    {
        public static readonly NLogManager Manager = new NLogManager();

        private NLogManager()
        {
        }

        public ILogger GetLogger(string name)
        {
            return new NLogLogger(NLog.LogManager.GetLogger(name));
        }
    }
}
