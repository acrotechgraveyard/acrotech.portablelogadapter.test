﻿using Acrotech.PortableLogAdapter.NLogAdapter;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using NLog.Config;
using System;

namespace Acrotech.PortableLogAdapter.Test.NLogAdapter
{
    [TestClass]
    public class NLogLoggerTest
    {
        public const string LoggerName = "UnitTest";
        private const string TestLogMessage = "Test";
        private const string TestLogMessageWithParams = "Test: {0}";
        public const LogLevel TestLogLevel = LogLevel.Debug;

        private LoggingConfiguration CreateConfig(Action<string> writeAction, NLog.LogLevel minLevel = null)
        {
            var config = new LoggingConfiguration();

            var target = new UnitTestTarget(writeAction);
            config.AddTarget("UnitTest", target);
            config.LoggingRules.Add(new LoggingRule("*", minLevel ?? NLog.LogLevel.Trace, target));

            return config;
        }

        [TestMethod]
        public void NameTest()
        {
            var logOutput = string.Empty;

            LogManager.Configuration = CreateConfig(x => logOutput = x);
            var logger = NLogManager.Default.GetLogger(LoggerName);

            Assert.IsNotNull(logger.Name);
            Assert.AreEqual(LoggerName, logger.Name);
        }

        [TestMethod]
        public void LogMessageTest()
        {
            var logOutput = string.Empty;

            LogManager.Configuration = CreateConfig(x => logOutput = x);
            var logger = NLogManager.Default.GetLogger(LoggerName);

            logger.Log(TestLogLevel, TestLogMessage);

            Assert.AreEqual(string.Format("[{0}] {1}", TestLogLevel, TestLogMessage), logOutput);
        }

        [TestMethod]
        public void LogWithMessageParamsTest()
        {
            var logOutput = string.Empty;

            LogManager.Configuration = CreateConfig(x => logOutput = x);
            var logger = NLogManager.Default.GetLogger(LoggerName);

            logger.Log(TestLogLevel, TestLogMessageWithParams, LoggerName);

            Assert.AreEqual(string.Format("[{0}] {1}: {2}", TestLogLevel, TestLogMessage, LoggerName), logOutput);
        }

        [TestMethod]
        public void LogWithMessageCreatorTest()
        {
            var logOutput = string.Empty;

            LogManager.Configuration = CreateConfig(x => logOutput = x);
            var logger = NLogManager.Default.GetLogger(LoggerName);

            logger.Log(TestLogLevel, () => TestLogMessage);

            Assert.AreEqual(string.Format("[{0}] {1}", TestLogLevel, TestLogMessage), logOutput);
        }

        [TestMethod]
        public void LogMessageWithFormatParamsWithoutParamArgsTest()
        {
            var logOutput = string.Empty;

            LogManager.Configuration = CreateConfig(x => logOutput = x);
            var logger = NLogManager.Default.GetLogger(LoggerName);

            logger.Log(TestLogLevel, TestLogMessageWithParams);

            Assert.AreEqual(string.Format("[{0}] {1}", TestLogLevel, TestLogMessageWithParams), logOutput);
        }

        [TestMethod]
        public void AllLogLevelsTest()
        {
            var logOutput = string.Empty;

            LogManager.Configuration = CreateConfig(x => logOutput = x);
            var logger = NLogManager.Default.GetLogger(LoggerName);

            foreach (LogLevel level in logger.GetAllLogLevels())
            {
                logger.Log(level, TestLogMessage);

                Assert.AreEqual(string.Format("[{0}] {1}", level, TestLogMessage), logOutput);

                logOutput = string.Empty;
            }
        }

        [TestMethod]
        public void LogLevelRestrictionTest()
        {
            var logOutput = string.Empty;

            var logger = NLogManager.Default.GetLogger(LoggerName);

            foreach (LogLevel logLevel in logger.GetAllLogLevels())
            {
                LogManager.Configuration = CreateConfig(x => logOutput = x, NLogLogger.Convert(logLevel));

                foreach (LogLevel testLevel in logger.GetAllLogLevels())
                {
                    logger.Log(testLevel, TestLogMessage);

                    if ((int)testLevel < (int)logLevel)
                    {
                        Assert.AreEqual(string.Empty, logOutput);
                    }
                    else
                    {
                        Assert.AreEqual(string.Format("[{0}] {1}", testLevel, TestLogMessage), logOutput);
                    }

                    logOutput = string.Empty;
                }
            }
        }

        [TestMethod]
        public void LogExceptionTest()
        {
            var logOutput = string.Empty;

            LogManager.Configuration = CreateConfig(x => logOutput = x);
            var logger = NLogManager.Default.GetLogger(LoggerName);

            logger.LogException(TestLogLevel, new Exception(LoggerName), TestLogMessage);

            Assert.AreEqual(string.Format("[{0}] {1}: System.Exception: {2}", TestLogLevel, TestLogMessage, LoggerName), logOutput);
        }

        [TestMethod]
        public void LogExceptionWithMessageParamsTest()
        {
            var logOutput = string.Empty;

            LogManager.Configuration = CreateConfig(x => logOutput = x);
            var logger = NLogManager.Default.GetLogger(LoggerName);

            logger.LogException(TestLogLevel, new Exception(LoggerName), TestLogMessageWithParams, LoggerName);

            Assert.AreEqual(string.Format("[{0}] {1}: {2}: System.Exception: {2}", TestLogLevel, TestLogMessage, LoggerName), logOutput);
        }

        [TestMethod]
        public void LogExceptionWithMessageCreatorTest()
        {
            var logOutput = string.Empty;

            LogManager.Configuration = CreateConfig(x => logOutput = x);
            var logger = NLogManager.Default.GetLogger(LoggerName);

            logger.LogException(TestLogLevel, new Exception(LoggerName), () => TestLogMessage);

            Assert.AreEqual(string.Format("[{0}] {1}: System.Exception: {2}", TestLogLevel, TestLogMessage, LoggerName), logOutput);
        }
    }
}
