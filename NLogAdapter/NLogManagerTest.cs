﻿using Acrotech.PortableLogAdapter.NLogAdapter;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Acrotech.PortableLogAdapter.Test.NLogAdapter
{
    [TestClass]
    public class NLogManagerTest
    {
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException), "NLog Doesn't Permit Null Logger Names")]
        public void GetLoggerNullNameTest()
        {
            var logger = NLogManager.Default.GetLogger(null);
        }

        [TestMethod]
        public void GetLoggerNonNullNameTest()
        {
            const string LoggerName = "UnitTest";

            var logger = NLogManager.Default.GetLogger(LoggerName);

            Assert.IsNotNull(logger);
            Assert.AreEqual(LoggerName, logger.Name);
        }
    }
}
