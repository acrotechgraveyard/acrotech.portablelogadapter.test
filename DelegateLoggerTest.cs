﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Acrotech.PortableLogAdapter;
using Telerik.JustMock;
using Acrotech.PortableLogAdapter.Loggers;

namespace Acrotech.PortableLogAdapter.Test
{
    [TestClass]
    public class DelegateLoggerTest
    {
        private const string LoggerName = "UnitTest";
        private const string TestLogMessage = "Test";
        private const string TestLogMessageWithParams = "Test: {0}";
        private const LogLevel TestLogLevel = LogLevel.Debug;

        [TestMethod]
        public void NameNullTest()
        {
            var logOutput = string.Empty;
            var logger = new DelegateLogger(null, TestLogLevel, x => logOutput = x);

            Assert.IsNull(logger.Name);

            logger.Log(TestLogLevel, TestLogMessage);

            Assert.AreEqual(string.Format("[{0}] {1}", TestLogLevel, TestLogMessage), logOutput);
        }

        [TestMethod]
        public void NameNonNullTest()
        {
            var logger = new DelegateLogger(LoggerName, TestLogLevel, null);

            Assert.IsNotNull(logger.Name);
            Assert.AreEqual(LoggerName, logger.Name);
        }

        [TestMethod]
        public void WriteActionNullTest()
        {
            var logOutput = string.Empty;
            var logger = new DelegateLogger(null, TestLogLevel, null);

            logger.Log(TestLogLevel, TestLogMessage);

            Assert.AreEqual(string.Empty, logOutput);
        }

        [TestMethod]
        public void WriteActionNonNullTest()
        {
            var logOutput = string.Empty;
            var logger = new DelegateLogger(null, TestLogLevel, x => logOutput = x);

            logger.Log(TestLogLevel, TestLogMessage);

            Assert.AreEqual(string.Format("[{0}] {1}", TestLogLevel, TestLogMessage), logOutput);
        }

        [TestMethod]
        public void LogWithMessageParamsTest()
        {
            var logOutput = string.Empty;
            var logger = new DelegateLogger(null, TestLogLevel, x => logOutput = x);

            logger.Log(TestLogLevel, TestLogMessageWithParams, LoggerName);

            Assert.AreEqual(string.Format("[{0}] {1}: {2}", TestLogLevel, TestLogMessage, LoggerName), logOutput);
        }

        [TestMethod]
        public void LogWithMessageCreatorTest()
        {
            var logOutput = string.Empty;
            var logger = new DelegateLogger(null, TestLogLevel, x => logOutput = x);

            logger.Log(TestLogLevel, () => TestLogMessage);

            Assert.AreEqual(string.Format("[{0}] {1}", TestLogLevel, TestLogMessage), logOutput);
        }

        [TestMethod]
        public void LogMessageWithFormatParamsWithoutParamArgsTest()
        {
            var logOutput = string.Empty;
            var logger = new DelegateLogger(null, TestLogLevel, x => logOutput = x);

            logger.Log(TestLogLevel, TestLogMessageWithParams);

            Assert.AreEqual(string.Format("[{0}] {1}", TestLogLevel, TestLogMessageWithParams), logOutput);
        }

        [TestMethod]
        public void AllLogLevelsTest()
        {
            var logOutput = string.Empty;
            var logger = new DelegateLogger(null, TestLogLevel, x => logOutput = x);

            foreach (LogLevel level in logger.GetAllLogLevels())
            {
                logger.Log(level, TestLogMessage);

                Assert.AreEqual(string.Format("[{0}] {1}", level, TestLogMessage), logOutput);

                logOutput = string.Empty;
            }
        }

        [TestMethod]
        public void LogExceptionTest()
        {
            var logOutput = string.Empty;
            var logger = new DelegateLogger(null, TestLogLevel, x => logOutput = x);

            logger.LogException(TestLogLevel, new Exception(LoggerName), TestLogMessage);

            Assert.AreEqual(string.Format("[{0}] {1}: System.Exception: {2}", TestLogLevel, TestLogMessage, LoggerName), logOutput);
        }

        [TestMethod]
        public void LogExceptionWithMessageParamsTest()
        {
            var logOutput = string.Empty;
            var logger = new DelegateLogger(null, TestLogLevel, x => logOutput = x);

            logger.LogException(TestLogLevel, new Exception(LoggerName), TestLogMessageWithParams, LoggerName);

            Assert.AreEqual(string.Format("[{0}] {1}: {2}: System.Exception: {2}", TestLogLevel, TestLogMessage, LoggerName), logOutput);
        }

        [TestMethod]
        public void LogExceptionWithMessageCreatorTest()
        {
            var logOutput = string.Empty;
            var logger = new DelegateLogger(null, TestLogLevel, x => logOutput = x);

            logger.LogException(TestLogLevel, new Exception(LoggerName), () => TestLogMessage);

            Assert.AreEqual(string.Format("[{0}] {1}: System.Exception: {2}", TestLogLevel, TestLogMessage, LoggerName), logOutput);
        }
    }
}
