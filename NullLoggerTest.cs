﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Acrotech.PortableLogAdapter;
using Telerik.JustMock;
using Acrotech.PortableLogAdapter.Loggers;

namespace Acrotech.PortableLogAdapter.Test
{
    [TestClass]
    public class NullLoggerTest
    {
        private const string LoggerName = "UnitTest";
        private const string TestLogMessage = "Test";
        private const string TestLogMessageWithParams = "Test: {0}";
        private const LogLevel TestLogLevel = LogLevel.Debug;

        [TestMethod]
        public void LogWithMessageParamsTest()
        {
            var logger = NullLogger.Default;

            logger.Log(TestLogLevel, TestLogMessageWithParams, LoggerName);
        }

        [TestMethod]
        public void LogWithMessageCreatorTest()
        {
            var logger = NullLogger.Default;

            logger.Log(TestLogLevel, () => TestLogMessage);
        }

        [TestMethod]
        public void LogMessageWithFormatParamsWithoutParamArgsTest()
        {
            var logger = NullLogger.Default;

            logger.Log(TestLogLevel, TestLogMessageWithParams);
        }

        [TestMethod]
        public void AllLogLevelsTest()
        {
            var logger = NullLogger.Default;

            foreach (LogLevel level in logger.GetAllLogLevels())
            {
                logger.Log(level, TestLogMessage);
            }
        }

        [TestMethod]
        public void LogExceptionTest()
        {
            var logger = NullLogger.Default;

            logger.LogException(TestLogLevel, new Exception(LoggerName), TestLogMessage);
        }

        [TestMethod]
        public void LogExceptionWithMessageParamsTest()
        {
            var logger = NullLogger.Default;

            logger.LogException(TestLogLevel, new Exception(LoggerName), TestLogMessageWithParams, LoggerName);
        }

        [TestMethod]
        public void LogExceptionWithMessageCreatorTest()
        {
            var logger = NullLogger.Default;

            logger.LogException(TestLogLevel, new Exception(LoggerName), () => TestLogMessage);
        }

        [TestMethod]
        public void IsEnabledTest()
        {
            var logger = NullLogger.Default;

            foreach (LogLevel level in logger.GetAllLogLevels())
            {
                Assert.IsFalse(logger.IsEnabled(level), "NullLogger should be disabled for all levels ({0} is enabled)", level);
            }
        }
    }
}
