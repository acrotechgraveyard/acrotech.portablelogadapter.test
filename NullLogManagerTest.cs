﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Acrotech.PortableLogAdapter;
using Acrotech.PortableLogAdapter.Managers;
using Acrotech.PortableLogAdapter.Loggers;

namespace Acrotech.PortableLogAdapter.Test
{
    [TestClass]
    public class NullLogManagerTest
    {
        [TestMethod]
        public void GetLoggerNullNameTest()
        {
            var logger = NullLogManager.Default.GetLogger(null);

            Assert.IsNotNull(logger);
            Assert.AreEqual(NullLogger.Default, logger);
            Assert.AreEqual(NullLogger.LoggerName, logger.Name);
        }

        [TestMethod]
        public void GetLoggerNonNullNameTest()
        {
            var logger = NullLogManager.Default.GetLogger("UnitTest");

            Assert.IsNotNull(logger);
            Assert.AreEqual(NullLogger.Default, logger);
            Assert.AreEqual(NullLogger.LoggerName, logger.Name);
        }

        [TestMethod]
        public void GetLoggerTest()
        {
            var logger = NullLogManager.Default.GetLogger();

            Assert.IsNotNull(logger);
            Assert.AreEqual(NullLogger.Default, logger);
            Assert.AreEqual(NullLogger.LoggerName, logger.Name);
        }
    }
}
