﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Acrotech.PortableLogAdapter;
using Acrotech.PortableLogAdapter.Managers;
using Acrotech.PortableLogAdapter.Loggers;

namespace Acrotech.PortableLogAdapter.Test
{
    [TestClass]
    public class ILogManagerTest
    {
        [TestMethod]
        public void GetLoggerNameTest()
        {
            Type type = null;

            Assert.AreEqual(string.Empty, type.GetLoggerName());
            Assert.AreEqual(typeof(ILogManagerTest).Name, typeof(ILogManagerTest).GetLoggerName());
        }

        [TestMethod]
        public void GetLoggerTest()
        {
            ILogManager manager = null;

            Assert.IsNull(manager.GetLogger());
            Assert.IsNull(manager.GetLogger(this));
            Assert.IsNull(manager.GetLogger(GetType()));
            Assert.IsNull(manager.GetLogger<ILogManagerTest>());

            manager = UnitTestLogManager.Default;
            ILogger logger = null;

            logger = manager.GetLogger();
            Assert.IsNotNull(logger);
            Assert.AreEqual(string.Empty, logger.Name);

            logger = manager.GetLogger(this);
            Assert.IsNotNull(logger);
            Assert.AreEqual(GetType().GetLoggerName(), logger.Name);

            logger = manager.GetLogger(GetType());
            Assert.IsNotNull(logger);
            Assert.AreEqual(GetType().GetLoggerName(), logger.Name);

            logger = manager.GetLogger<ILogManagerTest>();
            Assert.IsNotNull(logger);
            Assert.AreEqual(GetType().GetLoggerName(), logger.Name);
        }

        class UnitTestLogManager : ILogManager
        {
            public static readonly UnitTestLogManager Default = new UnitTestLogManager();

            #region ILogManager Members

            public ILogger GetLogger(string name)
            {
                return new UnitTestLogger(name);
            }

            #endregion
        }

        class UnitTestLogger : ILogger
        {
            public UnitTestLogger(string name)
            {
                Name = name;
            }

            #region ILogger Members

            public string Name { get; private set; }

            public void Log(LogLevel level, string format, params object[] args)
            {
            }

            public void Log(LogLevel level, Func<string> messageCreator)
            {
            }

            public void LogException(LogLevel level, Exception exception, string format, params object[] args)
            {
            }

            public void LogException(LogLevel level, Exception exception, Func<string> messageCreator)
            {
            }

            public LogLevel LogLevel { get; set; }

            #endregion
        }
    }
}
